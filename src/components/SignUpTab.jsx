import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle";
import "./style.css";
import validateEachInput from "../validateEachInput";

export class SignUpTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
      role: "",
      email: "",
      password: "",
      confirmPassword: "",
      hasAgreedToTerms: "",
      loginInSuccessful: false,
      errors:{}
    };
  }

  handleChange = (event) => {
    const {id, value} = event.target
    const error = validateEachInput(id, value, this.state.password);
    this.setState({ ...this.state,errors: {...this.state.errors, ...error}  , [id]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let result = Object.values(this.state.errors).filter(error => {
      if(error !== null)
      return error
    });
    if(result.length === 0 ){
      this.state.loginInSuccessful=true;
    }
    else{
      console.log("failed")
    }
  }

  render() {
    const { errors, loginInSuccessful} = this.state;
    return (
      loginInSuccessful ? <h1> Login Sucessful </h1>:
      <div className="col-md-10 card my-5 m-auto border-0 ">
        <div className="row g-0">
          <div className="col-md-4 col-sm col" >
            <img
              src="https://images.unsplash.com/photo-1525547719571-a2d4ac8945e2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1528&q=80"
              className="img-fluid rounded-start sign-in-img"
              alt="no found"
            />
          </div>
          <div className="col-md-8 col-sm-12 ">
            <div className="card-body">
              <h5 className="card-title">Sign Up</h5>
              <form onSubmit={this.handleSubmit}>
                <div className="name mx-5 d-flex">
                  <div className="mb-3 me-5">
                    <label htmlFor="firstName" className="form-label">
                      First Name
                    </label>
                    <input
                      type="FirstName"
                      className="form-control"
                      id="firstName"
                      required
                      onChange={this.handleChange}
                    />
                    {errors && errors.firstName && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.firstName)}</p>}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="lastName" className="form-label">
                      Last Name
                    </label>
                    <input
                      type="lastName"
                      className="form-control"
                      id="lastName"
                      onChange={this.handleChange}
                      required
                    />
                    {errors && errors.lastName && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.lastName)}</p>}
                  </div>
                </div>
                <div className="age-gender d-flex mx-5">
                  <div className="age mb-3">
                    <label htmlFor="age" className="form-label">
                      Age
                    </label>
                    <input
                      type="number"
                      onChange={this.handleChange}
                      className="form-control"
                      min={18}
                      max={100}
                      id="age"
                      required
                    />
                    {errors && errors.age && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.age)}</p>}
                  </div>
                  <div className="gender m-4">
                  <select
                    className="form-select input-small"
                    aria-label="Default select example"
                    id='gender'
                    onChange={this.handleChange}
                    defaultValue=""
                    required
                  >
                    <option value={""}>Gender</option>
                    <option value={"male"}>Male</option>
                    <option value={"female"}>Female</option>
                    <option value={'rather not to say'}>Rather not to say</option>
                  </select>
                  {errors && errors.gender && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.gender)}</p>}
                  </div>
                  <div className="role m-4">
                  <select
                    className="form-select input-small"
                    aria-label="Default select example"
                    defaultValue=""
                    id='role'
                    onChange={this.handleChange}
                    required
                  >
                    <option value="">Role</option>
                    <option value={'Developer'}>Developer</option>
                    <option value={'Senior Developer'}>Senior Developer</option>
                    <option value={'Lead Engineer'}>Lead Engineer</option>
                    <option value={'CTO'}>CTO</option>
                  </select>
                  {errors && errors.role && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.role)}</p>}
                  </div>
                </div>

                <div className="mb-3 mx-5 w-75">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Email address
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    aria-describedby="emailHelp"
                    onChange={this.handleChange}
                    required
                  />
                  {errors && errors.email && <p className="fs-9  position-absolute text-danger">{JSON.stringify(errors.email)}</p>}
                </div>
                <div className="mb-3 mx-5 w-75">
                  <label htmlFor="exampleInputPassword1" className="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    onChange={this.handleChange}
                    required
                  />
                  {errors && errors.password && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.password)}</p>}
                </div>
                <div className="mb-3 mx-5 w-75">
                  <label htmlFor="exampleInputPassword1" className="form-label">
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    id="confirmPassword"
                    onChange={this.handleChange}
                    required
                  />
                  {errors && errors.confirmPassword && <p className="fs-9 position-absolute text-danger">{JSON.stringify(errors.confirmPassword)}</p>}
                </div>
                <div className="border mb-3 mx-5 form-check">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="hasAgreedToTerms"
                    required
                  />
                  <label className="form-check-label" htmlFor="exampleCheck1">
                    I agree to all the terms and conditions
                  </label>
                </div>
                <button type="submit" className="btn btn-primary mx-5">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SignUpTab;
