import validator from "validator";

export default function validateEachInput(inputType, input, password) {

  switch (inputType) {
    case "firstName": {
      if (input.length === 0) {
        return { [inputType]: "cannot be empty" };
      } else if (!validator.isAlpha(input)) {
        return { [inputType]: "only alphabets" };
      } else {
        return { [inputType]: null };
      }
    }
    case "lastName": {
      if (input.length === 0) {
        return { [inputType]: "cannot be empty" };
      }
      else if (!validator.isAlpha(input)) {
        return { [inputType]: "only alphabets" };
      } else {
        return { [inputType]: null };
      }
    }
    case 'age':{
        if(input < 18 || input > 120){
            return {[inputType] : "Invalid age"}
        }else{
            return {[inputType] : null}
        }
    }
    case 'gender':{
        if(validator.isEmpty(input)){
            return {[inputType]: "select gender"}
        }else{
            return {[inputType]: null}
        }
    }
    case 'role':{
        if(validator.isEmpty(input)){
            return {[inputType]: "select role"}
        }else{
            return {[inputType]: null}
        }
    }
    case 'email':{
        if(!validator.isEmail(input)){
            return {[inputType]: 'invalid email'}
        }else{
            return {[inputType]: null}
        }
    }
    case 'password':{
        if(!validator.isStrongPassword(input)){
            return {[inputType]: 'must be atleast 8 characters long, contain at least one uppercase, one lowercase,one digit and one symbol'}
        }else{
            return {[inputType]: null}
        }
    }
    case 'confirmPassword':{
        if(!validator.equals(input, password)){
            return {[inputType]: 'password does not match'}
        }else{
            return {[inputType]: null}
        }
    }
    default:{
        return;
    }
  }
}
